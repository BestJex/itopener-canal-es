# itopener-canal-es

#### 项目介绍
基于canal，开发canal 客户端，接收mysql binlog解析的数据，然后根据变化的数据回源到数据库里去查询需要放入Elasticsearch的数据并保存到Elasticsearch。主要场景是在mysql的基础上进行数据异构到Elasticsearch，使用Elasticsearch提供查询服务，以便提高查询效率

#### 软件架构
使用spring boot 2.0.1.RELEASE进行开发。作为canal的客户端，基于Zookeeper自动发现canal的方式，从canal获取mysql binlog解析的数据并进行处理。工具主要分为几个模块：

- canal-es：启动服务模块，在此模块进行配置并启动
- canal-es-core：核心模块，作为canal客户端接收数据并调用对应的处理
- canal-es-ext：扩展模块，扩展对回源数据的查询和保存到Elasticsearch，以便支持自定义异构数据（里面有示例）
- canal-es-admin：管理模块，主要用于支持一些管理和统计功能。此模块暂时未开发。

#### 使用说明

这个工具主要是mysql数据到es数据的同步，主要的场景也就是数据异构，更多数情况是需要回源到数据库进行多表联合查询甚至一对多的查询，因此很难像otter一样通过管理页面来动态配置，该工具主要是针对于开发人员，需要定制开发异构同步数据的处理

canal相关文档请参考：https://github.com/alibaba/canal

此工具是使用的fork的分支（主要改动是将坐标类型的字段定义了一个值，而不是使用Types.OTHER,便于区分以及后续的处理）：https://github.com/dengfuwei/canal

1. 需要先启动canal服务端
2. 在canal-es-ext中扩展需要异构同步数据的处理，主要分为3个包
	- handler：定义是处理哪个数据库哪个表数据变更的事件，并且根据事件类型自行回源查询数据库和保存到es
	- model：与es中对应的类
	- repos：spring data elasticsearch的Repository，用于es的增删改操作
	
	如果需要多数据源配置，可以自行在canal-es中配置，然后在canal-es-ext中使用对应的jdbcTemplate即可
	
3. 在canal-es中修改相关的配置，然后启动，暂时只支持通过zk去发现canal服务端，计划以后也只支持这种方式，方便水平扩展部署节点
